package com.liangdu.web.rest;

import com.liangdu.LiangduApp;

import com.liangdu.domain.LdMenu;
import com.liangdu.repository.LdMenuRepository;
import com.liangdu.service.LdMenuService;
import com.liangdu.service.dto.LdMenuDTO;
import com.liangdu.service.mapper.LdMenuMapper;
import com.liangdu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.liangdu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LdMenuResource REST controller.
 *
 * @see LdMenuResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LiangduApp.class)
public class LdMenuResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final Integer DEFAULT_PID = 1;
    private static final Integer UPDATED_PID = 2;

    private static final Integer DEFAULT_ORDER = 1;
    private static final Integer UPDATED_ORDER = 2;

    private static final Integer DEFAULT_IS_SHOW = 1;
    private static final Integer UPDATED_IS_SHOW = 2;

    private static final Integer DEFAULT_IS_DEL = 1;
    private static final Integer UPDATED_IS_DEL = 2;

    @Autowired
    private LdMenuRepository ldMenuRepository;

    @Autowired
    private LdMenuMapper ldMenuMapper;

    @Autowired
    private LdMenuService ldMenuService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLdMenuMockMvc;

    private LdMenu ldMenu;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LdMenuResource ldMenuResource = new LdMenuResource(ldMenuService);
        this.restLdMenuMockMvc = MockMvcBuilders.standaloneSetup(ldMenuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LdMenu createEntity(EntityManager em) {
        LdMenu ldMenu = new LdMenu()
            .name(DEFAULT_NAME)
            .url(DEFAULT_URL)
            .pid(DEFAULT_PID)
            .order(DEFAULT_ORDER)
            .isShow(DEFAULT_IS_SHOW)
            .isDel(DEFAULT_IS_DEL);
        return ldMenu;
    }

    @Before
    public void initTest() {
        ldMenu = createEntity(em);
    }

    @Test
    @Transactional
    public void createLdMenu() throws Exception {
        int databaseSizeBeforeCreate = ldMenuRepository.findAll().size();

        // Create the LdMenu
        LdMenuDTO ldMenuDTO = ldMenuMapper.toDto(ldMenu);
        restLdMenuMockMvc.perform(post("/api/ld-menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ldMenuDTO)))
            .andExpect(status().isCreated());

        // Validate the LdMenu in the database
        List<LdMenu> ldMenuList = ldMenuRepository.findAll();
        assertThat(ldMenuList).hasSize(databaseSizeBeforeCreate + 1);
        LdMenu testLdMenu = ldMenuList.get(ldMenuList.size() - 1);
        assertThat(testLdMenu.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testLdMenu.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testLdMenu.getPid()).isEqualTo(DEFAULT_PID);
        assertThat(testLdMenu.getOrder()).isEqualTo(DEFAULT_ORDER);
        assertThat(testLdMenu.getIsShow()).isEqualTo(DEFAULT_IS_SHOW);
        assertThat(testLdMenu.getIsDel()).isEqualTo(DEFAULT_IS_DEL);
    }

    @Test
    @Transactional
    public void createLdMenuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ldMenuRepository.findAll().size();

        // Create the LdMenu with an existing ID
        ldMenu.setId(1L);
        LdMenuDTO ldMenuDTO = ldMenuMapper.toDto(ldMenu);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLdMenuMockMvc.perform(post("/api/ld-menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ldMenuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LdMenu in the database
        List<LdMenu> ldMenuList = ldMenuRepository.findAll();
        assertThat(ldMenuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLdMenus() throws Exception {
        // Initialize the database
        ldMenuRepository.saveAndFlush(ldMenu);

        // Get all the ldMenuList
        restLdMenuMockMvc.perform(get("/api/ld-menus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ldMenu.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].pid").value(hasItem(DEFAULT_PID)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)))
            .andExpect(jsonPath("$.[*].isShow").value(hasItem(DEFAULT_IS_SHOW)))
            .andExpect(jsonPath("$.[*].isDel").value(hasItem(DEFAULT_IS_DEL)));
    }

    @Test
    @Transactional
    public void getLdMenu() throws Exception {
        // Initialize the database
        ldMenuRepository.saveAndFlush(ldMenu);

        // Get the ldMenu
        restLdMenuMockMvc.perform(get("/api/ld-menus/{id}", ldMenu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ldMenu.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.pid").value(DEFAULT_PID))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER))
            .andExpect(jsonPath("$.isShow").value(DEFAULT_IS_SHOW))
            .andExpect(jsonPath("$.isDel").value(DEFAULT_IS_DEL));
    }

    @Test
    @Transactional
    public void getNonExistingLdMenu() throws Exception {
        // Get the ldMenu
        restLdMenuMockMvc.perform(get("/api/ld-menus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLdMenu() throws Exception {
        // Initialize the database
        ldMenuRepository.saveAndFlush(ldMenu);
        int databaseSizeBeforeUpdate = ldMenuRepository.findAll().size();

        // Update the ldMenu
        LdMenu updatedLdMenu = ldMenuRepository.findOne(ldMenu.getId());
        // Disconnect from session so that the updates on updatedLdMenu are not directly saved in db
        em.detach(updatedLdMenu);
        updatedLdMenu
            .name(UPDATED_NAME)
            .url(UPDATED_URL)
            .pid(UPDATED_PID)
            .order(UPDATED_ORDER)
            .isShow(UPDATED_IS_SHOW)
            .isDel(UPDATED_IS_DEL);
        LdMenuDTO ldMenuDTO = ldMenuMapper.toDto(updatedLdMenu);

        restLdMenuMockMvc.perform(put("/api/ld-menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ldMenuDTO)))
            .andExpect(status().isOk());

        // Validate the LdMenu in the database
        List<LdMenu> ldMenuList = ldMenuRepository.findAll();
        assertThat(ldMenuList).hasSize(databaseSizeBeforeUpdate);
        LdMenu testLdMenu = ldMenuList.get(ldMenuList.size() - 1);
        assertThat(testLdMenu.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testLdMenu.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testLdMenu.getPid()).isEqualTo(UPDATED_PID);
        assertThat(testLdMenu.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testLdMenu.getIsShow()).isEqualTo(UPDATED_IS_SHOW);
        assertThat(testLdMenu.getIsDel()).isEqualTo(UPDATED_IS_DEL);
    }

    @Test
    @Transactional
    public void updateNonExistingLdMenu() throws Exception {
        int databaseSizeBeforeUpdate = ldMenuRepository.findAll().size();

        // Create the LdMenu
        LdMenuDTO ldMenuDTO = ldMenuMapper.toDto(ldMenu);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLdMenuMockMvc.perform(put("/api/ld-menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ldMenuDTO)))
            .andExpect(status().isCreated());

        // Validate the LdMenu in the database
        List<LdMenu> ldMenuList = ldMenuRepository.findAll();
        assertThat(ldMenuList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLdMenu() throws Exception {
        // Initialize the database
        ldMenuRepository.saveAndFlush(ldMenu);
        int databaseSizeBeforeDelete = ldMenuRepository.findAll().size();

        // Get the ldMenu
        restLdMenuMockMvc.perform(delete("/api/ld-menus/{id}", ldMenu.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LdMenu> ldMenuList = ldMenuRepository.findAll();
        assertThat(ldMenuList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LdMenu.class);
        LdMenu ldMenu1 = new LdMenu();
        ldMenu1.setId(1L);
        LdMenu ldMenu2 = new LdMenu();
        ldMenu2.setId(ldMenu1.getId());
        assertThat(ldMenu1).isEqualTo(ldMenu2);
        ldMenu2.setId(2L);
        assertThat(ldMenu1).isNotEqualTo(ldMenu2);
        ldMenu1.setId(null);
        assertThat(ldMenu1).isNotEqualTo(ldMenu2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LdMenuDTO.class);
        LdMenuDTO ldMenuDTO1 = new LdMenuDTO();
        ldMenuDTO1.setId(1L);
        LdMenuDTO ldMenuDTO2 = new LdMenuDTO();
        assertThat(ldMenuDTO1).isNotEqualTo(ldMenuDTO2);
        ldMenuDTO2.setId(ldMenuDTO1.getId());
        assertThat(ldMenuDTO1).isEqualTo(ldMenuDTO2);
        ldMenuDTO2.setId(2L);
        assertThat(ldMenuDTO1).isNotEqualTo(ldMenuDTO2);
        ldMenuDTO1.setId(null);
        assertThat(ldMenuDTO1).isNotEqualTo(ldMenuDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(ldMenuMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(ldMenuMapper.fromId(null)).isNull();
    }
}
