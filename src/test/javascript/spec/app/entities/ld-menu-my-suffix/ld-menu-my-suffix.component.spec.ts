/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { LiangduTestModule } from '../../../test.module';
import { LdMenuMySuffixComponent } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix.component';
import { LdMenuMySuffixService } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix.service';
import { LdMenuMySuffix } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix.model';

describe('Component Tests', () => {

    describe('LdMenuMySuffix Management Component', () => {
        let comp: LdMenuMySuffixComponent;
        let fixture: ComponentFixture<LdMenuMySuffixComponent>;
        let service: LdMenuMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [LiangduTestModule],
                declarations: [LdMenuMySuffixComponent],
                providers: [
                    LdMenuMySuffixService
                ]
            })
            .overrideTemplate(LdMenuMySuffixComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LdMenuMySuffixComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LdMenuMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new LdMenuMySuffix(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.ldMenus[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
