/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LiangduTestModule } from '../../../test.module';
import { LdMenuMySuffixDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix-delete-dialog.component';
import { LdMenuMySuffixService } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix.service';

describe('Component Tests', () => {

    describe('LdMenuMySuffix Management Delete Component', () => {
        let comp: LdMenuMySuffixDeleteDialogComponent;
        let fixture: ComponentFixture<LdMenuMySuffixDeleteDialogComponent>;
        let service: LdMenuMySuffixService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [LiangduTestModule],
                declarations: [LdMenuMySuffixDeleteDialogComponent],
                providers: [
                    LdMenuMySuffixService
                ]
            })
            .overrideTemplate(LdMenuMySuffixDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LdMenuMySuffixDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LdMenuMySuffixService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
