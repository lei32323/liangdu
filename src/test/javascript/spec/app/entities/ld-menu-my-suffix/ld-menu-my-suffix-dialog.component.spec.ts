/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LiangduTestModule } from '../../../test.module';
import { LdMenuMySuffixDialogComponent } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix-dialog.component';
import { LdMenuMySuffixService } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix.service';
import { LdMenuMySuffix } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix.model';

describe('Component Tests', () => {

    describe('LdMenuMySuffix Management Dialog Component', () => {
        let comp: LdMenuMySuffixDialogComponent;
        let fixture: ComponentFixture<LdMenuMySuffixDialogComponent>;
        let service: LdMenuMySuffixService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [LiangduTestModule],
                declarations: [LdMenuMySuffixDialogComponent],
                providers: [
                    LdMenuMySuffixService
                ]
            })
            .overrideTemplate(LdMenuMySuffixDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LdMenuMySuffixDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LdMenuMySuffixService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LdMenuMySuffix(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.ldMenu = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'ldMenuListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LdMenuMySuffix();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.ldMenu = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'ldMenuListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
