/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { LiangduTestModule } from '../../../test.module';
import { LdMenuMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix-detail.component';
import { LdMenuMySuffixService } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix.service';
import { LdMenuMySuffix } from '../../../../../../main/webapp/app/entities/ld-menu-my-suffix/ld-menu-my-suffix.model';

describe('Component Tests', () => {

    describe('LdMenuMySuffix Management Detail Component', () => {
        let comp: LdMenuMySuffixDetailComponent;
        let fixture: ComponentFixture<LdMenuMySuffixDetailComponent>;
        let service: LdMenuMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [LiangduTestModule],
                declarations: [LdMenuMySuffixDetailComponent],
                providers: [
                    LdMenuMySuffixService
                ]
            })
            .overrideTemplate(LdMenuMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LdMenuMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LdMenuMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new LdMenuMySuffix(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.ldMenu).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
