package com.liangdu.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.liangdu.service.LdMenuService;
import com.liangdu.web.rest.errors.BadRequestAlertException;
import com.liangdu.web.rest.util.HeaderUtil;
import com.liangdu.service.dto.LdMenuDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LdMenu.
 */
@RestController
@RequestMapping("/api")
public class LdMenuResource {

    private final Logger log = LoggerFactory.getLogger(LdMenuResource.class);

    private static final String ENTITY_NAME = "ldMenu";

    private final LdMenuService ldMenuService;

    public LdMenuResource(LdMenuService ldMenuService) {
        this.ldMenuService = ldMenuService;
    }

    /**
     * POST  /ld-menus : Create a new ldMenu.
     *
     * @param ldMenuDTO the ldMenuDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ldMenuDTO, or with status 400 (Bad Request) if the ldMenu has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ld-menus")
    @Timed
    public ResponseEntity<LdMenuDTO> createLdMenu(@RequestBody LdMenuDTO ldMenuDTO) throws URISyntaxException {
        log.debug("REST request to save LdMenu : {}", ldMenuDTO);
        if (ldMenuDTO.getId() != null) {
            throw new BadRequestAlertException("A new ldMenu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LdMenuDTO result = ldMenuService.save(ldMenuDTO);
        return ResponseEntity.created(new URI("/api/ld-menus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ld-menus : Updates an existing ldMenu.
     *
     * @param ldMenuDTO the ldMenuDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ldMenuDTO,
     * or with status 400 (Bad Request) if the ldMenuDTO is not valid,
     * or with status 500 (Internal Server Error) if the ldMenuDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ld-menus")
    @Timed
    public ResponseEntity<LdMenuDTO> updateLdMenu(@RequestBody LdMenuDTO ldMenuDTO) throws URISyntaxException {
        log.debug("REST request to update LdMenu : {}", ldMenuDTO);
        if (ldMenuDTO.getId() == null) {
            return createLdMenu(ldMenuDTO);
        }
        LdMenuDTO result = ldMenuService.save(ldMenuDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ldMenuDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ld-menus : get all the ldMenus.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ldMenus in body
     */
    @GetMapping("/ld-menus")
    @Timed
    public List<LdMenuDTO> getAllLdMenus() {
        log.debug("REST request to get all LdMenus");
        return ldMenuService.findAll();
        }

    /**
     * GET  /ld-menus/:id : get the "id" ldMenu.
     *
     * @param id the id of the ldMenuDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ldMenuDTO, or with status 404 (Not Found)
     */
    @GetMapping("/ld-menus/{id}")
    @Timed
    public ResponseEntity<LdMenuDTO> getLdMenu(@PathVariable Long id) {
        log.debug("REST request to get LdMenu : {}", id);
        LdMenuDTO ldMenuDTO = ldMenuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ldMenuDTO));
    }

    /**
     * DELETE  /ld-menus/:id : delete the "id" ldMenu.
     *
     * @param id the id of the ldMenuDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ld-menus/{id}")
    @Timed
    public ResponseEntity<Void> deleteLdMenu(@PathVariable Long id) {
        log.debug("REST request to delete LdMenu : {}", id);
        ldMenuService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
