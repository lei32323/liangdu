/**
 * View Models used by Spring MVC REST controllers.
 */
package com.liangdu.web.rest.vm;
