package com.liangdu.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A LdMenu.
 */
@Entity
@Table(name = "ld_menu")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LdMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "url")
    private String url;

    @Column(name = "pid")
    private Integer pid;

    @Column(name = "jhi_order")
    private Integer order;

    @Column(name = "is_show")
    private Integer isShow;

    @Column(name = "is_del")
    private Integer isDel;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public LdMenu name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public LdMenu url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPid() {
        return pid;
    }

    public LdMenu pid(Integer pid) {
        this.pid = pid;
        return this;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getOrder() {
        return order;
    }

    public LdMenu order(Integer order) {
        this.order = order;
        return this;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public LdMenu isShow(Integer isShow) {
        this.isShow = isShow;
        return this;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public LdMenu isDel(Integer isDel) {
        this.isDel = isDel;
        return this;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LdMenu ldMenu = (LdMenu) o;
        if (ldMenu.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ldMenu.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LdMenu{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", pid=" + getPid() +
            ", order=" + getOrder() +
            ", isShow=" + getIsShow() +
            ", isDel=" + getIsDel() +
            "}";
    }
}
