package com.liangdu.service.mapper;

import com.liangdu.domain.*;
import com.liangdu.service.dto.LdMenuDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LdMenu and its DTO LdMenuDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LdMenuMapper extends EntityMapper<LdMenuDTO, LdMenu> {

    

    

    default LdMenu fromId(Long id) {
        if (id == null) {
            return null;
        }
        LdMenu ldMenu = new LdMenu();
        ldMenu.setId(id);
        return ldMenu;
    }
}
