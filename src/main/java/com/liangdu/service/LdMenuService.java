package com.liangdu.service;

import com.liangdu.service.dto.LdMenuDTO;
import java.util.List;

/**
 * Service Interface for managing LdMenu.
 */
public interface LdMenuService {

    /**
     * Save a ldMenu.
     *
     * @param ldMenuDTO the entity to save
     * @return the persisted entity
     */
    LdMenuDTO save(LdMenuDTO ldMenuDTO);

    /**
     * Get all the ldMenus.
     *
     * @return the list of entities
     */
    List<LdMenuDTO> findAll();

    /**
     * Get the "id" ldMenu.
     *
     * @param id the id of the entity
     * @return the entity
     */
    LdMenuDTO findOne(Long id);

    /**
     * Delete the "id" ldMenu.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
