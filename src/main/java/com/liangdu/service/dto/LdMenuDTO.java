package com.liangdu.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the LdMenu entity.
 */
public class LdMenuDTO implements Serializable {

    private Long id;

    private String name;

    private String url;

    private Integer pid;

    private Integer order;

    private Integer isShow;

    private Integer isDel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LdMenuDTO ldMenuDTO = (LdMenuDTO) o;
        if(ldMenuDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ldMenuDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LdMenuDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", pid=" + getPid() +
            ", order=" + getOrder() +
            ", isShow=" + getIsShow() +
            ", isDel=" + getIsDel() +
            "}";
    }
}
