package com.liangdu.service.impl;

import com.liangdu.service.LdMenuService;
import com.liangdu.domain.LdMenu;
import com.liangdu.repository.LdMenuRepository;
import com.liangdu.service.dto.LdMenuDTO;
import com.liangdu.service.mapper.LdMenuMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing LdMenu.
 */
@Service
@Transactional
public class LdMenuServiceImpl implements LdMenuService{

    private final Logger log = LoggerFactory.getLogger(LdMenuServiceImpl.class);

    private final LdMenuRepository ldMenuRepository;

    private final LdMenuMapper ldMenuMapper;

    public LdMenuServiceImpl(LdMenuRepository ldMenuRepository, LdMenuMapper ldMenuMapper) {
        this.ldMenuRepository = ldMenuRepository;
        this.ldMenuMapper = ldMenuMapper;
    }

    /**
     * Save a ldMenu.
     *
     * @param ldMenuDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LdMenuDTO save(LdMenuDTO ldMenuDTO) {
        log.debug("Request to save LdMenu : {}", ldMenuDTO);
        LdMenu ldMenu = ldMenuMapper.toEntity(ldMenuDTO);
        ldMenu = ldMenuRepository.save(ldMenu);
        return ldMenuMapper.toDto(ldMenu);
    }

    /**
     * Get all the ldMenus.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<LdMenuDTO> findAll() {
        log.debug("Request to get all LdMenus");
        return ldMenuRepository.findAll().stream()
            .map(ldMenuMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one ldMenu by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LdMenuDTO findOne(Long id) {
        log.debug("Request to get LdMenu : {}", id);
        LdMenu ldMenu = ldMenuRepository.findOne(id);
        return ldMenuMapper.toDto(ldMenu);
    }

    /**
     * Delete the ldMenu by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LdMenu : {}", id);
        ldMenuRepository.delete(id);
    }
}
