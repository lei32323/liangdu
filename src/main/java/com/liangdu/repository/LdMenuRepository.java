package com.liangdu.repository;

import com.liangdu.domain.LdMenu;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the LdMenu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LdMenuRepository extends JpaRepository<LdMenu, Long> {

}
