import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { LiangduLdMenuMySuffixModule } from './ld-menu-my-suffix/ld-menu-my-suffix.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        LiangduLdMenuMySuffixModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LiangduEntityModule {}
