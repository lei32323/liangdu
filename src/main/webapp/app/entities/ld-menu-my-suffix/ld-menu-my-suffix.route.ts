import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { LdMenuMySuffixComponent } from './ld-menu-my-suffix.component';
import { LdMenuMySuffixDetailComponent } from './ld-menu-my-suffix-detail.component';
import { LdMenuMySuffixPopupComponent } from './ld-menu-my-suffix-dialog.component';
import { LdMenuMySuffixDeletePopupComponent } from './ld-menu-my-suffix-delete-dialog.component';

export const ldMenuRoute: Routes = [
    {
        path: 'ld-menu-my-suffix',
        component: LdMenuMySuffixComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LdMenus'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ld-menu-my-suffix/:id',
        component: LdMenuMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LdMenus'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ldMenuPopupRoute: Routes = [
    {
        path: 'ld-menu-my-suffix-new',
        component: LdMenuMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LdMenus'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ld-menu-my-suffix/:id/edit',
        component: LdMenuMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LdMenus'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ld-menu-my-suffix/:id/delete',
        component: LdMenuMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LdMenus'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
