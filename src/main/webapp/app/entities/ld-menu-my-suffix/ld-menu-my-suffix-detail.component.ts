import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LdMenuMySuffix } from './ld-menu-my-suffix.model';
import { LdMenuMySuffixService } from './ld-menu-my-suffix.service';

@Component({
    selector: 'jhi-ld-menu-my-suffix-detail',
    templateUrl: './ld-menu-my-suffix-detail.component.html'
})
export class LdMenuMySuffixDetailComponent implements OnInit, OnDestroy {

    ldMenu: LdMenuMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ldMenuService: LdMenuMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLdMenus();
    }

    load(id) {
        this.ldMenuService.find(id).subscribe((ldMenu) => {
            this.ldMenu = ldMenu;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLdMenus() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ldMenuListModification',
            (response) => this.load(this.ldMenu.id)
        );
    }
}
