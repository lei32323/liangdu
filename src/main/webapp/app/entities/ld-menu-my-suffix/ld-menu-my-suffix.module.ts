import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LiangduSharedModule } from '../../shared';
import {
    LdMenuMySuffixService,
    LdMenuMySuffixPopupService,
    LdMenuMySuffixComponent,
    LdMenuMySuffixDetailComponent,
    LdMenuMySuffixDialogComponent,
    LdMenuMySuffixPopupComponent,
    LdMenuMySuffixDeletePopupComponent,
    LdMenuMySuffixDeleteDialogComponent,
    ldMenuRoute,
    ldMenuPopupRoute,
} from './';

const ENTITY_STATES = [
    ...ldMenuRoute,
    ...ldMenuPopupRoute,
];

@NgModule({
    imports: [
        LiangduSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LdMenuMySuffixComponent,
        LdMenuMySuffixDetailComponent,
        LdMenuMySuffixDialogComponent,
        LdMenuMySuffixDeleteDialogComponent,
        LdMenuMySuffixPopupComponent,
        LdMenuMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        LdMenuMySuffixComponent,
        LdMenuMySuffixDialogComponent,
        LdMenuMySuffixPopupComponent,
        LdMenuMySuffixDeleteDialogComponent,
        LdMenuMySuffixDeletePopupComponent,
    ],
    providers: [
        LdMenuMySuffixService,
        LdMenuMySuffixPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LiangduLdMenuMySuffixModule {}
