import { BaseEntity } from './../../shared';

export class LdMenuMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public url?: string,
        public pid?: number,
        public order?: number,
        public isShow?: number,
        public isDel?: number,
    ) {
    }
}
