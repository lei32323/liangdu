import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LdMenuMySuffix } from './ld-menu-my-suffix.model';
import { LdMenuMySuffixPopupService } from './ld-menu-my-suffix-popup.service';
import { LdMenuMySuffixService } from './ld-menu-my-suffix.service';

@Component({
    selector: 'jhi-ld-menu-my-suffix-delete-dialog',
    templateUrl: './ld-menu-my-suffix-delete-dialog.component.html'
})
export class LdMenuMySuffixDeleteDialogComponent {

    ldMenu: LdMenuMySuffix;

    constructor(
        private ldMenuService: LdMenuMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ldMenuService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ldMenuListModification',
                content: 'Deleted an ldMenu'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ld-menu-my-suffix-delete-popup',
    template: ''
})
export class LdMenuMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ldMenuPopupService: LdMenuMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ldMenuPopupService
                .open(LdMenuMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
