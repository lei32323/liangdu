import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LdMenuMySuffix } from './ld-menu-my-suffix.model';
import { LdMenuMySuffixPopupService } from './ld-menu-my-suffix-popup.service';
import { LdMenuMySuffixService } from './ld-menu-my-suffix.service';

@Component({
    selector: 'jhi-ld-menu-my-suffix-dialog',
    templateUrl: './ld-menu-my-suffix-dialog.component.html'
})
export class LdMenuMySuffixDialogComponent implements OnInit {

    ldMenu: LdMenuMySuffix;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private ldMenuService: LdMenuMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ldMenu.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ldMenuService.update(this.ldMenu));
        } else {
            this.subscribeToSaveResponse(
                this.ldMenuService.create(this.ldMenu));
        }
    }

    private subscribeToSaveResponse(result: Observable<LdMenuMySuffix>) {
        result.subscribe((res: LdMenuMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: LdMenuMySuffix) {
        this.eventManager.broadcast({ name: 'ldMenuListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-ld-menu-my-suffix-popup',
    template: ''
})
export class LdMenuMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ldMenuPopupService: LdMenuMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ldMenuPopupService
                    .open(LdMenuMySuffixDialogComponent as Component, params['id']);
            } else {
                this.ldMenuPopupService
                    .open(LdMenuMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
