import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LdMenuMySuffix } from './ld-menu-my-suffix.model';
import { LdMenuMySuffixService } from './ld-menu-my-suffix.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-ld-menu-my-suffix',
    templateUrl: './ld-menu-my-suffix.component.html'
})
export class LdMenuMySuffixComponent implements OnInit, OnDestroy {
ldMenus: LdMenuMySuffix[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private ldMenuService: LdMenuMySuffixService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.ldMenuService.query().subscribe(
            (res: ResponseWrapper) => {
                this.ldMenus = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInLdMenus();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: LdMenuMySuffix) {
        return item.id;
    }
    registerChangeInLdMenus() {
        this.eventSubscriber = this.eventManager.subscribe('ldMenuListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
