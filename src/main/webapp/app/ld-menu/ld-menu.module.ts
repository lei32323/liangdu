import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LdMenuComponent } from './ld-menu.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
      LdMenuComponent,
  ]
})
export class LdMenuModule {

}
