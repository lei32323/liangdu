import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ngx-webstorage';

import { LiangduSharedModule, UserRouteAccessService } from './shared';
import { LiangduAppRoutingModule} from './app-routing.module';
import { LiangduHomeModule } from './home/home.module';
import { LiangduAdminModule } from './admin/admin.module';
import { LiangduAccountModule } from './account/account.module';
import { LiangduEntityModule } from './entities/entity.module';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

import { LdMenuModule } from './ld-menu/ld-menu.module';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        LiangduAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        LiangduSharedModule,
        LiangduHomeModule,
        LiangduAdminModule,
        LiangduAccountModule,
        LiangduEntityModule,
        LdMenuModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class LiangduAppModule {}
